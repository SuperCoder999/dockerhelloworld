FROM nginx:alpine

RUN rm -rf /usr/share/nginx/html/*
COPY index.html /usr/share/nginx/html
COPY assets /usr/share/nginx/html/assets
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

EXPOSE 3000
